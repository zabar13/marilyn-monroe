import React from 'react';
import { shallow } from 'enzyme';

import Header from '../../src/components/Header';

test('renders correctly', () => {
    const tree = shallow(<Header/>);
    
    expect(tree).toMatchSnapshot();
})