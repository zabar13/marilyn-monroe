import React from 'react';
import { shallow } from 'enzyme';

import Description from '../../src/components/Description';

test('renders correctly', () => {
    const tree = shallow(<Description/>);
    
    expect(tree).toMatchSnapshot();
})
