import React from 'react';
import { shallow } from 'enzyme';

import { Photos } from '../../src/components/Photos';

test('renders correctly', () => {
    const tree = shallow(<Photos photos={[{ photo: '', link: '' }]} />);

    expect(tree).toMatchSnapshot();
})
