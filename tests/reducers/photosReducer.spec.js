import deepFreeze from 'deep-freeze';

import photosReducer, { ACTIONS } from '../../src/components/reducers/photosReducer';

test('should set photos', () => {
    const stateBefore = {
        photos: [],
        isLoading: false
    };

    const action = {
        type: ACTIONS.SET_PHOTOS,
        photos: [
            {
                photo: "http://test_m.jpg",
                link: "http://test.jpg"
            }
        ]
    };

    const stateAfter = {
        photos: [
            {
                photo: "http://test_m.jpg",
                link: "http://test.jpg"
            }
        ],
        isLoading: false
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(photosReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should reset photos reducer', () => {
    const stateBefore = {
        photos: [
            {
                photo: "http://test_m.jpg",
                link: "http://test.jpg"
            }
        ],
        isLoading: true
    };

    const action = {
        type: ACTIONS.RESET_PHOTOS
    };

    const stateAfter = {
        photos: [],
        isLoading: false
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(photosReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set isLoading to true', () => {
    const stateBefore = {
        photos: [],
        isLoading: false
    };

    const action = {
        type: ACTIONS.START_LOADER
    };

    const stateAfter = {
        photos: [],
        isLoading: true
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(photosReducer(stateBefore, action)).toEqual(stateAfter);
})

test('should set isLoading to false', () => {
    const stateBefore = {
        photos: [],
        isLoading: true
    };

    const action = {
        type: ACTIONS.STOP_LOADER
    };

    const stateAfter = {
        photos: [],
        isLoading: false
    };

    deepFreeze(stateBefore);
    deepFreeze(action);

    expect(photosReducer(stateBefore, action)).toEqual(stateAfter);
})