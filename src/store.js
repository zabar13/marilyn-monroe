import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import photosReducer from './components/reducers/photosReducer';

const store = createStore(combineReducers({
    photosReducer
}), applyMiddleware(thunk));

export default store;