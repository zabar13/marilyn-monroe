import { ACTIONS } from '../reducers/photosReducer';

export function setPhotos(photos) {
    return {
        type: ACTIONS.SET_PHOTOS,
        photos
    };
}

export function resetPhotos() {
    return {
        type: ACTIONS.RESET_PHOTOS
    };
}

export function startLoader() {
    return {
        type: ACTIONS.START_LOADER
    };
}

export function stopLoader() {
    return {
        type: ACTIONS.STOP_LOADER
    };
}