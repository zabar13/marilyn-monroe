import { setPhotos, startLoader, stopLoader } from './photosActionsCeators';

const MARILYN_SEARCH_URL = 'https://api.flickr.com/services/feeds/photos_public.gne?tags=Marilyn,Monroe&tagmode=all&format=json&callback=jsonFlickrFeed';

export function fetchAndSetData() {
    return async dispatch => {
        dispatch(startLoader());
        const responseData = await request(MARILYN_SEARCH_URL);
        dispatch(stopLoader());
        const newestPhotos = (responseData.items).map(photo => photo.media.m).splice(0, 9);
        const photos = newestPhotos.map(photo => {
            const link = generateLinkToBigPhoto(photo);
            return {
                photo, link
            }
        });

        dispatch(setPhotos(photos))
    };
}

function request(link) {
    return new Promise(resolve => {
        const script = document.createElement('script');

        script.src = link;

        window.jsonFlickrFeed = (data) => {
            resolve(data);
        };

        document.querySelector('head').appendChild(script);
    })
}

function generateLinkToBigPhoto(link) {

    const index = link.lastIndexOf('_m');

    return link.substring(0, index) + link.substring(index + 2, link.lenght);
}
