export const ACTIONS = {
    SET_PHOTOS: Symbol('SET_PHOTOS'),
    RESET_PHOTOS: Symbol('ACTIONS.RESET_PHOTOS'),
    START_LOADER: Symbol('START_LOADER'),
    STOP_LOADER: Symbol('STOP_LOADER')
};

const DEFAULT_STATE = {
    photos: [],
    isLoading: false
};

export default function photosReducer(state = DEFAULT_STATE, actions) {
    switch (actions.type) {
        case ACTIONS.SET_PHOTOS:
            return {
                ...state,
                photos: actions.photos
            };
        case ACTIONS.RESET_PHOTOS:
            return DEFAULT_STATE;
        case ACTIONS.START_LOADER:
            return {
                ...state,
                isLoading: true
            };
        case ACTIONS.STOP_LOADER:
            return {
                ...state,
                isLoading: false
            };
        default:
            return state
    }
}