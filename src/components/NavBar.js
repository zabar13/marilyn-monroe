import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';

export default function NavBar() {
    return (
        <div className='navbar'>
            <NavLink exact
                activeClassName='is-active'
                to='/'
            ><div className='btn-container'>
                    <div className='btn describe-btn'></div>
                </div>
            </NavLink>
            <NavLink exact
                activeClassName='is-active'
                to='/photos'
            ><div className='btn-container'>
                    <div className='btn photos-btn'></div>
                </div>
            </NavLink>
        </div>
    );
}