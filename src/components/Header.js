import React from 'react';
import NavBar from './NavBar';
import Avatar from './Avatar';

export default function Header() {
    return (
        <header className='header'>
            <NavBar />
            <Avatar />
        </header>
    );
}