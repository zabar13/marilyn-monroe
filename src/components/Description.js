import React from 'react';

export default function Description() {
    return (
        <div className='page description'>
            <div>
                <h2 className='description-name'>Marilyn Monroe</h2>
                <h5 className='description-info'>Denver,USA</h5>
            </div>
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p className='description-quote'>
                    Jestem samolubna, niecierpliwa i trochę niepewna siebie.
                    Popełniam błędy, tracę kontrolę i jestem czasami ciężka do zniesienia.
                    Ale jeśli nie potrafisz znieść mnie kiedy jestem najgorsza, to cholernie pewne, 
                    że nie zasługujesz na mnie, kiedy jestem najlepsza.<br />
                    <br />Marilyn Monroe
                </p>
                <p>
                    Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
                </p>
            </div>
        </div>
    );
}