import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Header from './Header';
import Description from './Description';
import Photos from './Photos';

export default class App extends Component {
    render() {
        return (
            <div>
                <Header />
                <Switch>
                    <Route
                        path='/'
                        exact
                        component={Description} />
                    <Route
                        path='/photos'
                        exact
                        component={Photos} />
                </Switch>
            </div>
        );
    }
}