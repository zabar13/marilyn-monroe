import React from 'react';

export default function Photo({ image }) {
    return (
        <div className='photo'>
            <img 
            onClick={()=>window.open(image.link,'','menubar=no')}
                src={image.photo} />
        </div>
    );
}