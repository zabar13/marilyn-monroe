import React, { Component } from 'react';
import { connect } from 'react-redux';

import Photo from './Photo';
import { fetchAndSetData } from './actions/photosActions';
import { resetPhotos } from './actions/photosActionsCeators';

export class Photos extends Component {
    componentDidMount = async () => {
        await this.props.fetchAndSetData();
    }

    componentWillUnmount = () => {
        this.props.resetPhotos();
    }

    render() {
        return (
            <div className='page photos-container' >
                {this.props.isLoading ?
                    <div className='loader'></div>
                    : ''}
                {this.props.photos.map((image, i) => <Photo key={i} image={image} />)}
            </div>
        );
    };
}

export default connect(state => state.photosReducer, {
    fetchAndSetData, resetPhotos
})(Photos)